# imdb series rating plotter

This script renders a bar plot of all the episodes of a given series found on IMDB

## Prerequisities

### R packages

    ggplot2
    viridis

You can install them with this command:

    $ R -e 'install.packages(c("ggplot2", "viridis"))'

### Data

Download and extract the data from https://datasets.imdbws.com/. You will need
title.basics.tsv.gz, title.episode.tsv.gz and title.ratings.tsv.gz.

You can do that with this command:

    $ wget https://datasets.imdbws.com/title.basics.tsv.gz; wget https://datasets.imdbws.com/title.episode.tsv.gz; wget https://datasets.imdbws.com/title.ratings.tsv.gz; gzip -d title.*.tsv.gz

### Script

Just chmod the script:

    $ chmod +x imdb.R

## Usage

The usage is really simple. Just use the series *tconst* as a command line
arguments. You can do multiple series at onece too (in fact, it is prefered,
since loading the db files can be time consuming). The *tconst* is the
*tt[0-9]+* id from the URL right after the */title/*.

Example:

    ./imdb.R tt2930604 tt0458290 tt8336340

